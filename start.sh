#!/bin/bash

function findJavaPath {
    echo "class FindJavaHome { public static void main(String[] args) { System.out.println(System.getProperty(\"java.home\")); } }" > /tmp/$$.findPath.java
    javac /tmp/$$.findPath.java 1>&2
    java -classpath /tmp FindJavaHome
    rm /tmp/$$.findPath.java /tmp/FindJavaHome.class 1>&2
}


SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PWD=`pwd`

echo "rebuilding server side scripts"
cd ${SCRIPT_DIR}/apache-tomcat-8.0.8/webapps/MedicalAnnotationTool/server_scripts
make
export CATALINA_HOME=${SCRIPT_DIR}/apache-tomcat-8.0.8
export JAVA_HOME=`findJavaPath`
export VIDEO_LIBRARY=${HOME}/video_library
export JAVA_OPTS="${JAVA_OPTS} -DVIDEO_LIBRARY=${VIDEO_LIBRARY}"
export CATALINA_OPTS="-Djava.net.preferIPv4Stack=true"
LOG_DIR=${CATALINA_HOME}/logs

if [ ! -d $LOG_DIR ] ; then
    mkdir $LOG_DIR
fi
if [ ! -d $VIDEO_LIBRARY ] ; then
    mkdir $VIDEO_LIBRARY
fi

command -v avconv >/dev/null 2>&1 || { echo >&2 "avconv is not installed.  Aborting."; exit -1; }

$SCRIPT_DIR/apache-tomcat-8.0.8/bin/startup.sh
cd $PWD

