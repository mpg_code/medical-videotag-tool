var dropZone;
var dropZoneTimer;

function initialize() {
    queryVideoFiles()
    initializeDropZone()
    initializePlayer()
    initializeAnnotationEditor()
}

function initializeDropZone() {
    //for the dropzone of the json file
    dropZone = document.getElementById('dropzone');
    dropZone.addEventListener('drop', handleFileSelect, false);
    dropZoneTimer = 0;

    var roundedBackground = document.getElementById('roundedBackground');
    roundedBackground.addEventListener('dragover', handleDragOver, false);
}

function showDropZone(show) {
    dropZone.style.visibility = (show == true) ? "visible" : "hidden";
}

function showPlayer(show) {
    var visibility = (show == true) ? "visible" : "hidden"
    canvasDraw.style.visibility = visibility
    canvasSeek.style.visibility = visibility
    currentVideo.style.visibility = visibility
    document.getElementById('playbackspeed').style.visibility = visibility
    document.getElementById('play-pause-button').style.visibility = visibility
    document.getElementById('stop-button').style.visibility = visibility
    document.getElementById('previous-button').style.visibility = visibility
    document.getElementById('next-button').style.visibility = visibility
}

//handle the file drag and drop selection from the user
function handleFileSelect(evt) {
    evt.stopPropagation();
    evt.preventDefault();

    var formData = new FormData();
    formData.append("file", event.dataTransfer.files[0]);
    var xhr = new XMLHttpRequest();
    xhr.upload.addEventListener("progress", uploadProgressChanged, false);
    xhr.addEventListener("load", uploadComplete, false);
    xhr.open("POST", "UploadServlet", true);
    xhr.send(formData);
}

function uploadProgressChanged(event) {
    uploadProgressBar.style.visibility = "visible"
    uploadProgressBar.value = Math.round(event.loaded / event.total * 100);
}

function uploadComplete(event) {
    console.log("upload complete!");
    uploadProgressBar.style.visibility = "hidden"
    window.setTimeout(queryVideoFiles, 500);
}

//handles the dragover
function handleDragOver(evt) {
    showDropZone(true);
    evt.stopPropagation();
    evt.preventDefault();
    evt.dataTransfer.dropEffect = 'copy';

    if (dropZoneTimer)
        window.clearTimeout(dropZoneTimer);
    dropZoneTimer = window.setTimeout(hideDropZone, 100);
}

function hideDropZone() {
    var video_src = document.getElementById('video').src;
    if ( video_src != null && video_src != "")
        showDropZone(false);
}

function handleKeyPress(e) {
    if (annotationEditorVisible)
        return;
    if (e.keyCode == 32) { // keycode of 'space'
        if (document.getElementById('video').paused)
            playVideo()
        else
            pauseVideo()

        // prevent the browser from scrolling on 'space'
        e.preventDefault()
    }
}

//the json object
var jsonObj;

//reference to the drawing canvas
var canvasDraw;
var contextCanvasDraw;

//reference to the video
var currentVideo;

//arrays for the replaying part for moving annotations
var startArray = [];
var stopArray =[];
//arrays for the replaying part for still annotations
var stillStartArray =[];
var stillStopArray =[];
var voiceRecArray = [];

var Edge = { NONE:0, NORTH:1, NORTHWEST:2, WEST:3, SOUTHWEST:4, SOUTH:5, SOUTHEAST:6, EAST:7, NORTHEAST:8 };

var currentAnnoStart;             //current moving annotations start time
var currentAnnoStop;              //current moving annotation stop time
var currentStillAnnoStart;        //video start time of the still annotation
var currentStillAnnoStop;         //video stop time of the still annotation
var currentStillAnnoStartTime;    //date start time of the still annotation
var currentStillAnnoStopTime;     //date stop time of the still annotation
var currentStillDrawTime;         //current time of the drawing part of the still annotation
var currentMarking = null;
var draggedMarking = null;
var hoveredMarking = null;
var hoveredEdge = Edge.NONE;
var resizingMarking = null;
var selectedMarking = null;

var canvasDirty = false;
var videoPausedForMarking = false;

//for the seekbar
var canvasSeek;
var contextSeek;

var s_PlaybackSpeed;
var s_PlaybackSpeedDisplay;

//Variables for the annotation part
// if something is drawn
var ISDRAWNONCE = 0;
//for mousedown
var MOUSEDOWN = 0;
//position of mouse down
var MOUSEDOWNPOS = {};
//position of mouse up
var MOUSEUPPOS = {};
//for the user drawing
var DRAWCOUNTER = 0;
//for in or out of annotation mode
var ANNOTATIONSTATE = 2;
//for the path redrawing
var PATHCOUNTER = 0;
//for if it is in still annotation mode
var ISINSTILLANNOTATIONMODE = 0;
//for the redrawing in time
var timeouts = [];
//array for pathes (contains all pathes)
var paths = [];
//array for the points of one pathes  (contains all points for one path)
var path = [];
//point of pathes
var points = [];
//array for all annotations for one video
var annotations = [];
// array for all the marks for one video
var marks = [];
//last still annotation
var lastStillStart = 0;
//array for seeking
var startStopTimes = [];

// name of the currently loaded video
var loadedVideoName = "";

var video_width;
var video_height;

function selectMarking(marking)
{
    saveAnnotation();
    selectedMarking = marking;
    displayAnnotationEditor(!!selectedMarking);
}

function initializePlayer() {
    canvasDraw = document.getElementById('canvasDraw');
    contextCanvasDraw = canvasDraw.getContext('2d');
    canvasDraw.addEventListener('dragover', handleDragOver, false);
    window.addEventListener('keypress', handleKeyPress, true);

    currentVideo = document.getElementById('video');

    canvasSeek = document.getElementById('canvasSeek');
    contextSeek = canvasSeek.getContext('2d');

    s_PlaybackSpeed = document.getElementById("s_PlaybackSpeed");
    s_PlaybackSpeedDisplay = document.getElementById("s_PlaybackSpeedDisplay");

    s_PlaybackSpeed.addEventListener('input', function (e){
        document.getElementById("video").playbackRate = s_PlaybackSpeed.value;
    });

    canvasSeek.addEventListener('click',seekToTimeOnSeekBar,false);
    currentVideo.addEventListener('canplay', videoLoaded,true);
    currentVideo.addEventListener('timeupdate', redraw, true);
    canvasDraw.onmousedown = handleMouseDown;
    canvasDraw.onmousemove = handleMouseMove;
    canvasDraw.onmouseup = handleMouseUp;

    currentVideo.onpause = function() {
        document.getElementById('play-pause-button').style.backgroundPosition = "0px 0"
    }
    currentVideo.onplaying = function() {
        document.getElementById('play-pause-button').style.backgroundPosition = "-50px 0"
    }

    showPlayer(false)
}

function updatePlaybackSpeed(value)
{
   s_PlaybackSpeedDisplay.value = value
}

function Marking(x, y, width, height, startTime) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.startTime = startTime;
    this.stopTime = 0;
    this.hovered = false;
    this.borderWidth = 5;
    this.geometryChanged = true;
    this.geometryUpdates = []
    this.centerX = x;
    this.centerY = y;

    function GeometryUpdate(time, x, y, width, height) {
        this.update = function(x, y, width, height) {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }

        this.setData = function(dataObject) {
            for (var prop in dataObject)
                this[prop] = dataObject[prop]
        }

        this.time = time;
        this.update(x, y, width, height);
    }

    this.setData = function(dataObject) {
        // Iterate over all the properties and adopt them for the new created Marking object.
        // This is necessary in order to get the javascript functions from the Marking prototype as well.
        for (var prop in dataObject) {
            if (prop == "geometryUpdates") {
                var updateData = dataObject[prop]
                for (var update in updateData) {
                    var geometryUpdate = new GeometryUpdate
                    geometryUpdate.setData(updateData[update])
                    this.geometryUpdates.push(geometryUpdate);
                }
            } else
                this[prop] = dataObject[prop];
        }
    }

    this.storeGeometryUpdate = function() {
        if (!this.geometryChanged) return;

        var last = this.geometryUpdates[this.geometryUpdates.length - 1];
        if (last && last.x == this.x && last.y == this.y && last.width == this.width && last.height == this.height)
            return;

        var geometryUpdate = null;
        for (var i in this.geometryUpdates) {
            if (this.geometryUpdates[i].time == currentVideo.currentTime) {
                geometryUpdate = this.geometryUpdates[i];
                break;
            }
        }

        this.sortEdges();
        if (geometryUpdate)
            geometryUpdate.update(this.x, this.y, this.width, this.height);
        else
            this.geometryUpdates.push(new GeometryUpdate(currentVideo.currentTime, this.x, this.y, this.width, this.height));
        this.geometryChanged = false;
    }

    this.resize = function(edge, offsetX, offsetY) {
        switch (edge) {
            case Edge.NORTHWEST:
                this.x += offsetX;
                this.width -= offsetX;
            case Edge.NORTH:
                this.y += offsetY;
                this.height -= offsetY;
                break;
            case Edge.SOUTHWEST:
                this.height += offsetY;
            case Edge.WEST:
                this.x += offsetX;
                this.width -= offsetX;
                break;
            case Edge.SOUTHEAST:
                this.width += offsetX;
            case Edge.SOUTH:
                this.height += offsetY;
                break;
            case Edge.NORTHEAST:
                this.y += offsetY;
                this.height -= offsetY;
            case Edge.EAST:
                this.width += offsetX;
                break;
            default: break;
        }
        this.geometryChanged = true;
    }

    this.updateDimensions = function(currentX, currentY) {
        var dx = currentX - this.centerX;
        var dy = currentY - this.centerY;
        var r = Math.sqrt(dx*dx + dy*dy);

        this.x = this.centerX - r
        this.y = this.centerY - r
        this.width = 2 * r
        this.height = 2 * r
        // this.width = currentX > this.x ? currentX - this.x : -(this.x - currentX);
        // this.height = currentY > this.y ? currentY - this.y : -(this.y - currentY);
        this.geometryChanged = true;
    }

    this.move = function(offsetX, offsetY) {
        this.x += offsetX;
        this.y += offsetY;
        this.geometryChanged = true;
    }

    this.setStopTime = function(stopTime) {
        this.stopTime = stopTime;
    }

    this.isSelected = function() {
        return selectedMarking == this;
    }

    this.hover = function(hovered) {
        this.hovered = hovered;
    }

    this.findGeometryAtTime = function(time) {
        var geometry = null;
        if (time) {
            for (var i in this.geometryUpdates) {
                var updateTime = this.geometryUpdates[i].time;
                if (!geometry || (updateTime > geometry.time && updateTime <= time))
                    geometry = this.geometryUpdates[i];
            }
        }
        return geometry;
    }

    this.paint = function(context, time) {
        var topLeftX = this.x;
        var topLeftY = this.y;
        var width = this.width;
        var height = this.height;

        if (this.hasOwnProperty("singleShot") && this.singleShot) {
            if (time != this.startTime)
                return;
        } else {
            var geometry = this.findGeometryAtTime(time);
            if (geometry && this != draggedMarking && this != resizingMarking) {
                topLeftX = geometry.x;
                topLeftY = geometry.y;
                width = geometry.width;
                height = geometry.height;
            }
        }


        if (this.isSelected() || this.hovered) {
            context.beginPath();
            context.lineWidth = 2
            context.strokeStyle = '#000000';
            context.rect(topLeftX, topLeftY, width, height);
            context.stroke();
            context.closePath();
        }


        this.paintEllipse(context, topLeftX + width / 2, topLeftY + height / 2, width, height)
    }

    this.paintEllipse = function(context, centerX, centerY, width, height) {
        context.beginPath();

        context.lineWidth = this.borderWidth;

        if (this.isSelected())
            context.strokeStyle = '#ff0000';
        else if (this.hovered)
            context.strokeStyle = '#ffff00';
        else
            context.strokeStyle = '#00ff00';

        var r_x = width / 2;
        var r_y = height / 2;
        for (var i = 0 * Math.PI; i < 2 * Math.PI; i += 0.01 ) {
            xPos = centerX - (r_y * Math.sin(i)) * Math.sin(0 * Math.PI) + (r_x * Math.cos(i)) * Math.cos(0 * Math.PI);
            yPos = centerY + (r_x * Math.cos(i)) * Math.sin(0 * Math.PI) + (r_y * Math.sin(i)) * Math.cos(0 * Math.PI);

            if (i == 0) {
                context.moveTo(xPos, yPos);
            } else {
                context.lineTo(xPos, yPos);
            }
        }
        context.stroke();
        context.closePath();
    }

    // Sort the edges of the rectangle to make sure that x/y always refers to the top left corner.
    this.sortEdges = function() {
        if (this.width < 0) {
            this.x = this.x + this.width;
            this.width = -this.width;
        }
        if (this.height < 0) {
            this.y = this.y + this.height;
            this.height = -this.height;
        }
    }

    this.encloses = function(pos) {
        var geometry = this.findGeometryAtTime(currentVideo.currentTime);
        if (!geometry) geometry = this;
        return (geometry.x <= pos.x && geometry.y <= pos.y && (geometry.x + geometry.width) >= pos.x && (geometry.y + geometry.height) >= pos.y)
    }

    this.getEdge = function(pos) {
        var resizeTop = false;
        var resizeBottom = false;
        var resizeLeft = false;
        var resizeRight = false;

        var geometry = this.findGeometryAtTime(currentVideo.currentTime);
        if (!geometry) geometry = this;

        if (geometry.x <= pos.x && (geometry.x + this.borderWidth) >= pos.x)
            resizeLeft = true;
        else if ((geometry.x + geometry.width - this.borderWidth) <= pos.x && (geometry.x + geometry.width) >= pos.x)
            resizeRight = true;

        if (geometry.y <= pos.y && (geometry.y + this.borderWidth) >= pos.y)
            resizeTop = true;
        else if ((geometry.y + geometry.height - this.borderWidth) <= pos.y && (geometry.y + geometry.height) >= pos.y)
            resizeBottom = true;

        if (resizeTop) {
            if (resizeLeft) return Edge.NORTHWEST;
            else if (resizeRight) return Edge.NORTHEAST;
            return Edge.NORTH;
        } else if (resizeBottom) {
            if (resizeLeft) return Edge.SOUTHWEST;
            else if (resizeRight) return Edge.SOUTHEAST;
            return Edge.SOUTH;
        } else {
            if (resizeLeft) return Edge.WEST;
            else if (resizeRight) return Edge.EAST;
        }
        return Edge.NONE; 
    }

    this.updateGeometryFromData = function() {
        var geometry = this.findGeometryAtTime(currentVideo.currentTime);
        if (!geometry) return;
        this.x = geometry.x;
        this.y = geometry.y;
        this.width = geometry.width;
        this.height = geometry.height;
    }


    this.isSingleShot = function() {
        return (this.hasOwnProperty("singleShot") && this.singleShot == true)
    }
}

    function getCursorForEdge(edge)
    {
        switch (edge) {
            case Edge.NORTH: return 's-resize';
            case Edge.NORTHWEST: return 'se-resize';
            case Edge.WEST: return 'e-resize';
            case Edge.SOUTHWEST: return 'ne-resize';
            case Edge.SOUTH: return 'n-resize';
            case Edge.SOUTHEAST: return 'nw-resize';
            case Edge.EAST: return 'w-resize';
            case Edge.NORTHEAST: return 'sw-resize';
            default: return 'default';
        }
    }
 
    function hitTestMarking(pos)
    {
        for (var i in marks) {
            var mark = marks[i];
            if (mark.encloses(pos) == true
                && mark.startTime == currentVideo.currentTime)
            // For now a marking is only active at the exact startTime
            //    && mark.startTime <= currentVideo.currentTime
            //    && (mark.stopTime == 0 || mark.stopTime >= currentVideo.currentTime))
                return mark;
        }
        return null;
    }

    function paintMarkings(context, clear)
    {
        if (clear)
            context.clearRect(0, 0, video_width, video_height);
        if (currentMarking) {
            currentMarking.paint(context);
        }
        for (var i in marks) {
            if (currentVideo.currentTime >= marks[i].startTime && (marks[i].stopTime == 0 || currentVideo.currentTime <= marks[i].stopTime))
                marks[i].paint(context, currentVideo.currentTime);
        }
        canvasDirty = false;
    }
// </script>
// <script>
    //buttons click events
    //mousedown on the play button

    function seekToTimeOnSeekBar(event) {
        saveAnnotation();
        var tab = new Array(event.clientX,event.clientY);
        var mousePosition = getMousePosition(tab,canvasSeek);

        var timeToJump = mousePosition[0]*video.duration/canvasSeek.width;
        video.currentTime = timeToJump;
    }


    // Sends annotations information to the Server for writing in JSON
    function writeJSON() {
        var payload = JSON.stringify(marks);
        var urlencoded = encodeURIComponent(payload);
        console.log("json: " + payload);
        console.log("urlencoded: " + urlencoded);

        var args = "video=" + loadedVideoName;
        args += "&json=" + urlencoded;

        var xhr_object=new XMLHttpRequest();
        xhr_object.open("POST","WriteJSONServlet",false);

        xhr_object.onreadystatechange  = function() {
             if(xhr_object.readyState  == 4) { }
        };
        xhr_object.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr_object.send(args);
    }

    window.addEventListener("keydown", function(e) {
        if ((e.keyCode == 46) && selectedMarking) { // 46 is delete, 8 is backspace
            if (hoveredMarking == selectedMarking) hoveredMarking = null;
            for (var i in marks) {
                if (marks[i] == selectedMarking) {
                    if (selectedMarking.startTime == currentVideo.currentTime)
                        marks.splice(i, 1);
                    else
                        selectedMarking.stopTime = currentVideo.currentTime;
                    break;
                }
            }
            selectMarking(null);
            paintMarkings(contextCanvasDraw, true);
        }
    }, true);


    function log(msg) {
        console.log(msg);
    }

    function videoLoaded(){
        video_width = currentVideo.videoWidth;
        video_height = currentVideo.videoHeight;
       
        var max_video_width = 900;
        if (video_width > max_video_width) {
            var ratio = video_width / max_video_width;
            video_width = max_video_width;
            video_height /= ratio;
	}

        //size settings
        document.getElementById("canvasDraw").height = video_height;
        document.getElementById("canvasDraw").width  = video_width;
        document.getElementById("video").height = video_height;
        document.getElementById("video").width  = video_width;
        document.getElementById("media-controls").width  = video_width;
        document.getElementById("canvasSeek").style.top = "" + (video_height+38) + "px";

        //set the pencil of the users drawing panel
        canvasDrawing.setPencil();
        //initalize the width of the seekbar for the current video
        seekDraw.draw(video_width);
        
        paintMarkings(contextCanvasDraw, true);
    }

    function redraw() {
        //only do something if the video is not paused
        if (currentVideo.paused)
           return;

        paintMarkings(contextCanvasDraw, true);
        seekDraw.draw(video_width);
    }

    //object for the seekbar drawing
    var seekDraw = {
        //draws the seekbar
        draw: function(width) {
            canvasSeek.width=video_width;
            contextSeek.strokeStyle = "#0071B9";
            contextSeek.fillStyle = "#C8C8C8";
            contextSeek.beginPath();
            contextSeek.rect(0, 0, canvasSeek.width, 50);
            contextSeek.closePath();
            contextSeek.stroke();
            contextSeek.fill();


            for (var i in marks) {
                var marking = marks[i];
                if (marking.isSingleShot()) {
                    seekDraw.drawSingleShot(canvasSeek.width, marking.startTime)
                } else {
                    seekDraw.drawAnnotations(canvasSeek.width, marking.startTime, marking.stopTime ? marking.stopTime : currentVideo.currentTime)
                }
            }
            seekDraw.drawThumb(canvasSeek.width)
        },
         //updates the annotations in the seekbar
        drawAnnotations: function(width,start,stop){
            //log("seek");
            contextSeek.strokeStyle = "#FF0000";
            contextSeek.lineWidth = 50;
            contextSeek.beginPath();
            contextSeek.moveTo((width/currentVideo.duration)*start,0);
            contextSeek.lineTo((width/currentVideo.duration)*stop,0);
            contextSeek.stroke();
            contextSeek.closePath();
        },
        drawSingleShot: function(width, timestamp){
            contextSeek.strokeStyle = "#FF0000";
            contextSeek.lineWidth = 50;
            contextSeek.beginPath();
            var seekPos = (width/currentVideo.duration) * timestamp;
            contextSeek.moveTo(seekPos,0);
            contextSeek.lineTo(seekPos+10,0);
            contextSeek.stroke();
            contextSeek.closePath();
        }, 
        drawThumb: function(width){
            contextSeek.strokeStyle = "#004966";
            contextSeek.lineWidth = 50;
            contextSeek.beginPath();
            contextSeek.moveTo((width/currentVideo.duration)*currentVideo.currentTime,0);
            contextSeek.lineTo(((width/currentVideo.duration)*currentVideo.currentTime)+10,0);
            contextSeek.stroke();
        }
    };

    //object for the drawing of the user
    var canvasDrawing = {
        //setup the pencil
        setPencil: function (){
            contextCanvasDraw.strokeStyle = '#00ff00';
            contextCanvasDraw.lineWidth = 5;
            contextCanvasDraw.lineCap = 'round';},
            //clear the drawing panel
            clearDrawingPaneel: function (){contextCanvasDraw.clearRect(0, 0, video_width, video_height);
        }
    };

    function handleMouseDown(e) {
        if (ANNOTATIONSTATE == 0) return;

        //starts the collection of the drawing points
        MOUSEDOWN=1;
        MOUSEDOWNPOS = windowToCanvas(canvasDraw, e.clientX, e.clientY);

        if (ANNOTATIONSTATE == 2) {
            // Pause the video and remember if it was playing
            videoPausedForMarking = !document.getElementById('video').paused;
            if (videoPausedForMarking) pauseVideo();

            // If there is no marking at this position, create a new one.
            var marking =  hitTestMarking(MOUSEDOWNPOS);
            if (!marking) {
                currentMarking = new Marking(MOUSEDOWNPOS.x, MOUSEDOWNPOS.y, 0, 0, currentVideo.currentTime);
                return;
            }

            // If the cursor is positioned on an edge, start resizing.
            hoveredEdge = marking.getEdge(MOUSEDOWNPOS);
            if (hoveredEdge != Edge.NONE) {
                document.getElementById('canvasDraw').style.cursor = getCursorForEdge(hoveredEdge);
                resizingMarking = marking;
                resizingMarking.updateGeometryFromData();
                return;
            }

            // Start dragging of the marking otherwise.
            draggedMarking = marking;
            draggedMarking.updateGeometryFromData();
        }
    } 

    function handleMouseUp(e){
        MOUSEUPPOS = windowToCanvas(canvasDraw, e.clientX, e.clientY);
        if (ANNOTATIONSTATE==1){
            //close the path an save everthing
            contextCanvasDraw.closePath();
            paths.push(points);
            path = [];
            points = [];
            DRAWCOUNTER = 0;
        } else if (ANNOTATIONSTATE==2) {

            if (MOUSEDOWNPOS.x == MOUSEUPPOS.x && MOUSEDOWNPOS.y == MOUSEUPPOS.y) {
                selectMarking(null);
                if (hoveredMarking) selectMarking(hoveredMarking);
                if (currentMarking) currentMarking = null; // delete the just created Marking instance.
            }
            var deltaX = MOUSEUPPOS.x - MOUSEDOWNPOS.x;
            var deltaY = MOUSEUPPOS.y - MOUSEDOWNPOS.y;
            if (draggedMarking) {
                draggedMarking.move(deltaX, deltaY);
                draggedMarking.storeGeometryUpdate();
                draggedMarking = null;
            } else if (resizingMarking) {
                resizingMarking.resize(hoveredEdge, deltaX, deltaY)
                resizingMarking.storeGeometryUpdate();
                resizingMarking = null;
                dirty()
            } else if (currentMarking) {
                currentMarking.storeGeometryUpdate();
                marks.push(currentMarking);
                selectMarking(currentMarking)
                currentMarking = null;
                dirty()
            }
            paintMarkings(contextCanvasDraw, true);
        }
        MOUSEDOWN=0;
    }

    function handleMouseMove(e) {
        //cacluate the diff from window to canvas
        var loc = windowToCanvas(canvasDraw, e.clientX, e.clientY);
        if (ANNOTATIONSTATE == 1) {
            if (MOUSEDOWN==1&&currentVideo.paused!=true) {
                //drawcounter is null a new path starts
                if (DRAWCOUNTER==0) {
                    contextCanvasDraw.beginPath();
                    points.push({x: loc.x, y: loc.y, t: currentVideo.currentTime});
                    contextCanvasDraw.moveTo(loc.x, loc.y);
                    DRAWCOUNTER++;
                }
                //draw counter not null countinue the path
                else if (DRAWCOUNTER>0&&currentVideo.paused!=true) {
                    points.push({x: loc.x, y: loc.y, t: currentVideo.currentTime});
                    contextCanvasDraw.quadraticCurveTo(loc.x, loc.y, loc.x, loc.y);
                    contextCanvasDraw.stroke();
                }
            }
            else if(MOUSEDOWN==1&&currentVideo.paused==true){
                //drawcounter is null a new path starts
                if (DRAWCOUNTER==0) {
                    contextCanvasDraw.beginPath();
                    currentStillDrawTime = new Date();
                  //  log('currentannotstart'+currentAnnoStart)
                  //  log('currentStillAnnoStartTime'+currentStillAnnoStartTime)
                  //  log('')
                  //  log(((currentAnnoStart+(currentStillDrawTime.getTime()-currentStillAnnoStartTime.getTime()))));
                    points.push({x: loc.x, y: loc.y, t: ((currentAnnoStart+(currentStillDrawTime.getTime()-currentStillAnnoStartTime.getTime()))/1000)});
                    contextCanvasDraw.moveTo(loc.x, loc.y);
                    DRAWCOUNTER++;
                }
                //draw counter not null countinue the path
                else if (DRAWCOUNTER>0&&currentVideo.paused==true) {
                    currentStillDrawTime = new Date();
                 //   log(((currentAnnoStart+(currentStillDrawTime.getTime()-currentStillAnnoStartTime.getTime()))));
                    points.push({x: loc.x, y: loc.y, t: ((currentAnnoStart+(currentStillDrawTime.getTime()-currentStillAnnoStartTime.getTime()))/1000)});
                    contextCanvasDraw.quadraticCurveTo(loc.x, loc.y, loc.x, loc.y);
                    contextCanvasDraw.stroke();
                }
            }
        } else if (ANNOTATIONSTATE == 2) {
            if (resizingMarking) {
                resizingMarking.resize(hoveredEdge, loc.x - MOUSEDOWNPOS.x, loc.y - MOUSEDOWNPOS.y);
                // We update the MOUSEDOWNPOS as we do not want to apply the same offset multiple times.
                MOUSEDOWNPOS = loc;
                paintMarkings(contextCanvasDraw, true);
                return;
            }

            if (draggedMarking) {
                draggedMarking.move(loc.x - MOUSEDOWNPOS.x, loc.y - MOUSEDOWNPOS.y);
                // We update the MOUSEDOWNPOS as we do not want to apply the same offset multiple times.
                MOUSEDOWNPOS = loc;
                paintMarkings(contextCanvasDraw, true);
                return;
            }

            // If the new location is still contained in the currently hovered marking, we skip hit testing.
            if (!hoveredMarking || !hoveredMarking.encloses(loc)) {
                if (hoveredMarking != null) hoveredMarking.hover(false) // reset previous hover
                hoveredMarking = hitTestMarking(loc);
                if (hoveredMarking != null) hoveredMarking.hover(true);
                canvasDirty = true;
            }

            // Update the cursor
            if (hoveredMarking) {
                hoveredEdge = hoveredMarking.getEdge(loc);
                document.getElementById('canvasDraw').style.cursor = getCursorForEdge(hoveredEdge);
            } else {
                document.getElementById('canvasDraw').style.cursor = 'crosshair';
            }

            if (currentMarking && MOUSEDOWN == 1) {
                currentMarking.updateDimensions(loc.x, loc.y);
                canvasDirty = true;
            }

            if (canvasDirty) paintMarkings(contextCanvasDraw, true); 
        }
    }
// </script>
// <script>
    //computes window to canvas coordinates
    function windowToCanvas(canvas, x, y){
        var bbox = canvas.getBoundingClientRect();
        return { x: x - bbox.left * (canvas.width / bbox.width),
            y: y  - bbox.top * (canvas.height / bbox.height)
        };
    }

    //loads the videoelement from file
    function loadVideo(videoURL){
        currentVideo.src = videoURL;
        currentVideo.load();
        loadJSONForVideo(loadedVideoName);
        showDropZone(false);
        showPlayer(true);
    }

    function loadJSONForVideo(videoName)
    {
        marks = [];
        var http = new XMLHttpRequest();
        http.onreadystatechange = function() {
            if (http.readyState == 4 && http.status == 200) {
                console.log("got json:" + http.responseText)
                var parsedJSON = JSON.parse(http.responseText);
                // This should give us an array of type 'Marking'
                for (var i in parsedJSON) {
                    var marking = new Marking;
                    dataObject = parsedJSON[i]
                    marking.setData(dataObject)
                    marks.push(marking);
                }
            }
        }

        var args = "videoName=" + videoName;
        http.open("POST", "GetJSONForVideoServlet", true);
        http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        http.send(args);
    }

    //the play button, starts the video
    function playVideo()
    {
        saveAnnotation();
        if (selectedMarking)
            selectMarking(null)
        if (document.getElementById('video').paused&&ISINSTILLANNOTATIONMODE!=1){
            ISDRAWNONCE=0;
            document.getElementById('video').play();
            // background-position:-19px 0;

            // To prevent the contextual menu from opening on right click
            canvasDraw.addEventListener('contextmenu', function(ev) { 
                ev.preventDefault(); 
                //alert('success!'); 
                return false; 
            }, false);
        } else {
            pauseVideo();
        }
    }

    //the pause button pauses the video
    function pauseVideo(){
        saveAnnotation();
        if (ANNOTATIONSTATE!=1&&ISINSTILLANNOTATIONMODE!=1)    {
        if (document.getElementById('video').played) {
            document.getElementById('video').pause();

            //clear the timer array (cancel all drawings)
            for (var i = 0; i < timeouts.length; i++) {
                clearTimeout(timeouts[i]);
            }

            timeouts = [];

            }
        }
      }

    //the backtostart button seeks back to start
    function stopVideo() {
        saveAnnotation();

        startStopTimes = [];

        for (var x = 0;x<startArray.length;x++){
            startStopTimes.push({start: startArray[x], stop: stopArray[x]})
        }

        //Sort the startArray for seeking
        sortedstartStopTimes = startStopTimes.slice();
        sortedstartStopTimes.sort(function(a, b){return a.start- b.start});

        if (ANNOTATIONSTATE!=1)    {

            //Reset some variables
            lastStillStart = 0; //resetts the last still start
            var seekToTime = 0;
            ISINSTILLANNOTATIONMODE=0;

            for (var i = 0; i < timeouts.length; i++) {
                clearTimeout(timeouts[i]);
            }
            pauseVideo();
            document.getElementById('video').currentTime = 0;
            ISDRAWNONCE=0;
        }

        paintMarkings(contextCanvasDraw, true);
    }

    function previousMarking() {
        saveAnnotation();
        var previousMarkingTime = -1;
        var marking = null;
        for (var i in marks) {
            var m = marks[i];
            if (m.startTime >= previousMarkingTime && m.startTime < currentVideo.currentTime) {
                previousMarkingTime = m.startTime
                marking = m;
            }
        }
        if (previousMarkingTime != -1)
            currentVideo.currentTime = previousMarkingTime;
        if (marking)
            selectMarking(marking)
        paintMarkings(contextCanvasDraw, true);
    }

    function nextMarking() {
        saveAnnotation();
        var nextMarkingTime = currentVideo.duration+1; 
        var marking = null;
        for (var i in marks) {
            var m = marks[i];
            if (m.startTime <= nextMarkingTime && m.startTime > currentVideo.currentTime) {
                nextMarkingTime = m.startTime
                marking = m;
            }
        }
        if (nextMarkingTime <= currentVideo.duration)
            currentVideo.currentTime = nextMarkingTime;
        if (marking)
            selectMarking(marking)
        paintMarkings(contextCanvasDraw, true);
    }
// </script>
// <script>
    /* Functions that allow to get the position of user's mouse */
    function getMousePosition(coord,obj) {
        var scroll = new Array((document.documentElement && document.documentElement.scrollLeft) || window.pageXOffset || self.pageXOffset
            || document.body.scrollLeft, (document.documentElement && document.documentElement.scrollTop)
            || window.pageYOffset || self.pageYOffset || document.body.scrollTop);
        var offset = findPosition(obj);
        var mouseVals= Array(coord[0] + scroll[0] - document.body.clientLeft -offset[0],coord[1] + scroll[1] - document.body.clientTop-offset[1]);
        return mouseVals;
    }


    function findPosition(obj) {
        var curleft = curtop = 0;
        if (obj.offsetParent) {
            do {
                curleft += obj.offsetLeft;
                curtop += obj.offsetTop;
            } while (obj = obj.offsetParent);
        }
        return [curleft,curtop];
    }
// </script>
