import java.io.IOException;
import java.util.Vector;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FilenameFilter;
import java.io.FileOutputStream;
import java.io.File;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet("/listVideosServlet")
public class ListVideosServlet extends HttpServlet {
    private static String s_videoDirectory = System.getProperty("user.home") + File.separator + "video_library";
    public void init() throws ServletException
    {

    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        File directory = new File(s_videoDirectory);
        if (!directory.exists())
            return;

        class Mp4Filter implements FilenameFilter {
            public boolean accept(File dir, String name) {
                return (name.toLowerCase().endsWith(".mp4"));
            }
        };

        File[] files = directory.listFiles(new Mp4Filter());
        Integer num_encoding = 0;
        for (int i = 0; i < files.length; i++) {
            if (files[i].isFile()) {
                String filename = files[i].getName();
                filename = filename.substring(0, filename.lastIndexOf('.'));
                // If we have a lock file the the video is not ready yet.
                if (new File(files[i].getAbsolutePath() + ".lock").exists()) {
                    out.println("<div class=\"blinking\" style=\"cursor: not-allowed;\">" + filename + " (encoding)</div>");
                    ++num_encoding;
                } else 
                    out.println("<div onclick=\"selectFile('" + files[i].getName() + "')\">" + filename + "</div>");
            }
        }
        response.addHeader("num_encoding", num_encoding.toString());
        out.close();
    }

    public void destroy()
    {
    }
}

