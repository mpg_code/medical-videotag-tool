import java.io.IOException;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.BufferedReader;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet("/getJSONForVideoServlet")
public class GetJSONForVideoServlet extends HttpServlet {
    private static String s_videoDirectory = System.getProperty("user.home") + File.separator + "video_library";
    public void init() throws ServletException
    {

    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        System.out.println("+++++videoName:" + request.getParameter("videoName"));
        File jsonFile = new File(s_videoDirectory + File.separator + request.getParameter("videoName") + ".json");
        if (jsonFile.exists()) {
            BufferedReader reader = new BufferedReader(new FileReader(jsonFile));
            String line = null;
            while ((line = reader.readLine()) != null)
                out.println(line);
        } else {
            out.println("[]");
        }
        out.close();
    }

    public void destroy()
    {
    }
}

