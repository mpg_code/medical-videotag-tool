

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LikeLineServlet
 */
public class LikeLineServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static DatabaseConnector dc = null;
	private int binSize = 1;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LikeLineServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if (dc == null) dc = new DatabaseConnector();
		Statement st = dc.getStatement();
		
		// Receiving a Like
		String video = request.getParameter("video");
		String time = request.getParameter("time");
		
		int K = Integer.parseInt(request.getParameter("k"));
		double alpha = Double.parseDouble(request.getParameter("alpha"));
		
		System.out.println(video + " " + time);
		// Storing it into the database
		try {
			if (!time.equals(""))
				st.executeUpdate("INSERT INTO LikeLine VALUE(0,'"+video+"'," + time + ",CONCAT(CURDATE(),' ',CURTIME()));");
			
			// Finding the number of Likes on the same video
			ResultSet rs = st.executeQuery("SELECT count(id) from LikeLine where video = '" + video + "';");
			rs.next();
			int nbLikes = rs.getInt(1);
			
			
			// Sending it back
			PrintWriter pw = response.getWriter();
			
			if (K < nbLikes) {

				double[] likes_old = new double[nbLikes-K];
				double[] likes_new = new double[K];
				// Finding all Likes on the same video
				rs = st.executeQuery("SELECT time from LikeLine where video = '" + video + "';");
				int ind_like = 0;
				while(rs.next() && ind_like<(nbLikes-K)) {
					likes_old[ind_like] = rs.getDouble(1);
					ind_like++;
				}
				while(rs.next()) {
					likes_new[ind_like-nbLikes+K] = rs.getDouble(1);
					ind_like++;
				}

	
				double[] likeMap_Old = computeLikeMap(likes_old); 
				double[] likeMap_New = computeLikeMap(likes_new); 
					
				int max_ind = (int) max(likeMap_Old.length,likeMap_New.length);
				pw.print(max_ind);
				pw.print("\n");
				for (int i=0;i<max_ind;i++) {
					pw.print(i);
					pw.print(" ");
					if (i>=likeMap_Old.length) {
						pw.print((1-alpha)*likeMap_New[i]);
					} else if (i>=likeMap_New.length) {
						pw.print(alpha*likeMap_Old[i]);
					} else {
						pw.print(alpha*likeMap_Old[i] + (1-alpha)*likeMap_New[i]);
					}
					pw.print("\n");
				}
			} else {
				double[] likes = new double[nbLikes];
				// Finding all Likes on the same video
				rs = st.executeQuery("SELECT time from LikeLine where video = '" + video + "';");
				int ind_like = 0;
				while(rs.next()) {
					likes[ind_like] = rs.getDouble(1);
					ind_like++;
				}

	
				double[] likeMap = computeLikeMap(likes); 
					
				int max_ind = likeMap.length;
				pw.print(max_ind);
				pw.print("\n");
				for (int i=0;i<max_ind;i++) {
					pw.print(i);
					pw.print(" ");
					pw.print(likeMap[i]);
					pw.print("\n");
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public double[] computeLikeMap(double[] likes) {
		//double[] res = null;
		
		double max = 0.0;
		for (int i=0;i<likes.length;i++){
			if (likes[i]>max) max = likes[i];
		}
		
		
		//double[] arr_int = new double[(int) Math.floor(max/binSize)+1];
		double[] res = new double[(int) Math.floor(max/binSize)+4]; // To handle gaussian kernels
		for (int i=0;i<res.length;i++) {
			res[i] = 0;
		}
	

		double max_bin = 0.0;
		for (int j=0;j<likes.length;j++) {
			double curr = likes[j];
			
			double start = Math.floor(curr);
			for (double i = max(start-2,0.0); i < min(start+3,res.length) ; i=i+0.1) {
				int index = (int) Math.floor(i/binSize);
				
				res[index] += getGaussianValue(start-i);
				if (res[index] > max_bin) max_bin = res[index];
			}

		}
		
		for (int i=0;i<res.length;i++){
			if (max_bin>0) {
				res[i] = res[i]/max_bin;
			} else {
				res[i] = 0;
			}
			
		}

		return res;
	}
	
	
	public double getGaussianValue(double x) {
		return Math.exp(-x*x/2)/Math.sqrt(2*Math.PI);
	}
	
	public double min(double a, double b) {
		double res = a;
		if (b<a) res = b;
		return res;
	}
	
	public double max(double a, double b) {
		double res = a;
		if (b>a) res = b;
		return res;
	}

}
