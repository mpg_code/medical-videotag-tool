import java.io.IOException;
import java.util.Vector;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.ProcessBuilder;
import java.lang.ProcessBuilder.Redirect;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@MultipartConfig
@WebServlet("/uploadServlet")
public class UploadServlet extends HttpServlet {
    private static String s_home = System.getProperty("user.home");
    private static Logger LOGGER = Logger.getLogger("InfoLogging");

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Part uploaded_file = request.getPart("file");
        String filename = getFilename(uploaded_file);
        InputStream inputStream = uploaded_file.getInputStream();

        // strip the suffix from filename
        int suffix_pos = filename.lastIndexOf('.');
        if (suffix_pos > 0)
            filename = filename.substring(0, suffix_pos);

        // create video directory if it does not exist yet
        File uploadDirectory = new File(s_home + File.separator + "upload");
        if (!uploadDirectory.exists()) uploadDirectory.mkdirs();

        // add directory path to filename
        String upload_filename = uploadDirectory.getAbsolutePath() + File.separator + filename;

        // if the file exists already, we add a suffix
        File file = new File(upload_filename);
        int suffix = 0;
        while (file.exists())
            file = new File(upload_filename + "." + ++suffix);
        file.createNewFile();
        upload_filename = file.getAbsolutePath();

        OutputStream outputStream = new FileOutputStream(file);

        int read = 0;
        byte[] bytes = new byte[1024];
        while ((read = inputStream.read(bytes)) != -1) outputStream.write(bytes, 0, read);

        response.setContentType("text/plain");
        response.setCharacterEncoding("UTF-8");
        //response.getWriter().write("File " + upload_filename + " successfully uploaded.");

        File videoDirectory = new File(s_home + File.separator + "video_library");
        if (!videoDirectory.exists()) videoDirectory.mkdirs();

        // add directory path to filename
        String converted_filename = videoDirectory.getAbsolutePath() + File.separator + filename;
        if (suffix > 0)
            converted_filename = converted_filename + "." + suffix;
        // The suffix .mp4 is required for avconv to choose the right encoder.
        converted_filename = converted_filename + ".mp4";

        try {
            String convertVideo = System.getenv("CATALINA_HOME") + File.separator + "webapps/MedicalAnnotationTool/server_scripts/convertVideo.sh";
            ProcessBuilder pb = new ProcessBuilder("nohup", convertVideo, upload_filename, converted_filename);
            pb.directory(new File("."));
            pb.redirectErrorStream(true);
            pb.redirectOutput(Redirect.PIPE);
            Process p = pb.start();
            response.getWriter().write("encoding job scheduled.");
        } catch (Exception e) {
            response.getWriter().write("Encoding failed: " + e.toString());
        }
    }

    private static String getFilename(Part part) {
        for (String cd : part.getHeader("content-disposition").split(";")) {
            if (cd.trim().startsWith("filename")) {
                String filename = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
                return filename.substring(filename.lastIndexOf('/') + 1).substring(filename.lastIndexOf('\\') + 1); // MSIE fix.
            }
        }
        return null;
    }

    


}
