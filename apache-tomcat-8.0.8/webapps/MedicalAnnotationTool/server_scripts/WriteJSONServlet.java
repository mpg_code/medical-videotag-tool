
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class WriteJSONServlet
 */
public class WriteJSONServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static String s_videoDirectory = System.getProperty("user.home") + File.separator + "video_library";

    /**
     * @see HttpServlet#HttpServlet()
     */
    public WriteJSONServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String video = request.getParameter("video");
        String encoding = request.getCharacterEncoding();
        String json = java.net.URLDecoder.decode(request.getParameter("json"), encoding != null ? encoding : "UTF-8");
        System.out.println("json (decoded):" + json);

        String jsonFilePath = s_videoDirectory + File.separator + video + ".json";
        File jsonFile = new File(jsonFilePath);
        if (jsonFile.exists()) {
            Date now = new Date();
            SimpleDateFormat ft_now = new SimpleDateFormat("yyyy.MM.dd-hh:mm:ss");
            jsonFile.renameTo(new File(jsonFilePath + "." + ft_now.format(now)));
            jsonFile = new File(jsonFilePath);
        }

        PrintWriter writer = new PrintWriter(jsonFile);
        writer.print(json);
        writer.close();
    }

}
