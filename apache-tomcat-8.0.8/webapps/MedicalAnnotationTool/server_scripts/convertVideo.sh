#!/bin/bash

if [ $# != 2 ] ; then
    echo "usage: $0 [input] [output]"
    echo "Use $0 to decode the [input] video from any codec to h.264 stored in [output]"
    exit -1;
fi

LOCK="${2}.lock"
echo $LOCK
touch "$LOCK"

IS_H264=`avconv -i ${1} 2>&1 | grep Stream | grep Video | grep h264`
if [ -n "$IS_H264" ]; then
    echo "==== uploaded file $1 is h264 already. ====" >> /tmp/convert.stdout
    cp "$1" "$2"
else
    echo "=====[stdout] avconv -i $1 -cv libx264 $s =====" >> /tmp/convert.stdout
    echo "=====[stderr] avconv -i $1 -cv libx264 $s =====" >> /tmp/convert.stderr
    avconv -i "$1" -c:v libx264 "$2" >> /tmp/convert.stdout 2>> /tmp/convert.stderr
fi

rm "$LOCK"

