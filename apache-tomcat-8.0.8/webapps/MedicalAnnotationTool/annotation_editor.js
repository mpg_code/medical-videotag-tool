var annotationEditorVisible = false;
var modalLayer;
var annotationEditor;
var annotationClassificationElement;
var annotationDescriptionElement;
var annotationsDirty;
var saveButton = null;
var saveInterval = null;

function dirty()
{
    annotationsDirty = true;
    saveButton.style.backgroundColor = "#ff0000";
    saveButton.innerHTML = "Saving Changes..."
}

function initializeAnnotationEditor()
{
    modalLayer = document.getElementById("modalLayer");
    annotationEditor = document.getElementById("annotationEditor");
    annotationDescriptionElement = document.getElementById("annotationDescription")
    saveButton = document.getElementById("saveButton")
    annotationsDirty = false;
    saveInterval = null;

    annotationClassificationElement = completely(document.getElementById('classificationContainer'));
    var combination = [
        { id: '',    options:[ 'polyp','bleeding','tumor' ] }
    ];

    annotationClassificationElement.onChange = function(text) {
        dirty()
        // search the matching combination.
        for (var i=0;i<combination.length;i++) {
           if (text.indexOf(combination[i].id)===0) {
               annotationClassificationElement.startFrom = combination[i].id.length;
               annotationClassificationElement.options =   combination[i].options;
               annotationClassificationElement.repaint();
               return;
           }
        }
    }

    annotationDescriptionElement.oninput = dirty
}

function displayAnnotationEditor(displayEditor)
{
    saveAnnotation()
    if (selectedMarking) {
        var editorBoundingBox = annotationEditor.getBoundingClientRect();
        var editorAbsX = document.documentElement.clientWidth - editorBoundingBox.width - 12;
        var markingAbsX = canvasDraw.getBoundingClientRect().left + selectedMarking.x;

        var overlapLeft = editorBoundingBox.width - markingAbsX + 12;
        var overlapRight = markingAbsX + selectedMarking.width - editorAbsX;
        if (overlapLeft < 0) overlapLeft = 0;
        if (overlapRight < 0) overlapRight = 0;
        annotationEditor.style.right = (overlapLeft < overlapRight) ? null : '12px';
        annotationEditor.style.left = (overlapLeft < overlapRight) ? '12px' : null;
    }
    annotationEditor.style.visibility = displayEditor ? "visible" : "hidden"
    annotationEditorVisible = displayEditor
    if (saveInterval)
        window.clearInterval(saveInterval)
    if (!selectedMarking)
        return;

    annotationClassificationElement.setText(selectedMarking.hasOwnProperty("classification") ? selectedMarking["classification"] : "")
    annotationDescriptionElement.value = selectedMarking.hasOwnProperty("description") ? selectedMarking["description"] : ""

    saveInterval=setInterval(saveAnnotation, 2000);
}

function saveAnnotation()
{
    if (!annotationsDirty)
        return;
    selectedMarking["classification"] = annotationClassificationElement.getText()
    selectedMarking["description"] = annotationDescription.value
    selectedMarking["singleShot"] = true;
    writeJSON()
    saveButton.style.backgroundColor = "#D6D6D6";
    saveButton.innerHTML = "Changes Saved"
    annotationsDirty = false;
}


