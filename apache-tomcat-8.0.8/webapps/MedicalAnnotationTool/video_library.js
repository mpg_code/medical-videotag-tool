
var xhr_videoFiles = new XMLHttpRequest();
xhr_videoFiles.onreadystatechange = function() {
    if (xhr_videoFiles.readyState==4 && xhr_videoFiles.status==200) {
        var videoListElement = document.getElementById("videoList");
        if (videoListElement.innerHTML != xhr_videoFiles.responseText)
            videoListElement.innerHTML = xhr_videoFiles.responseText;

        // There is still something encoding. Query again in a few seconds
        if (xhr_videoFiles.getResponseHeader("num_encoding") > 0)
            window.setTimeout(queryVideoFiles, 6000)
    }
}

function queryVideoFiles() {
    xhr_videoFiles.open("POST", "ListVideosServlet", true);
    xhr_videoFiles.setRequestHeader("Content-type", "application/x-www-form-urlencoded"); 
    xhr_videoFiles.send();
}

function selectFile(filename) {
    loadedVideoName = filename;
    loadVideo("../VideoPublisher/" + filename);
}
